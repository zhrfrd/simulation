function setDyslexia() {
    alert("Press 'ESC' on your keyboard to exit the simulation");

    //Check all the elements of the page and return the text nodes
    var getTextNodes = function(element) {
        return $(element).find(":not(iframe,script)").addBack().contents().filter(function() {
            return this.nodeType == 3; //.nodeType = 3 --> Text
        });
    };

    var textNodes = getTextNodes($("body"));   //Consider all the elements of the page

    function isLetter(char) {
        //Return true if char is a letter
        //Return false is char is not a letter
        return /^[\d]$/.test(char);   // ^[\d]$ (Metacharacters to find if the string starts and ends with a digit
    }

    var wordsInTextNodes = [];

    //Check for each word in the page
    for (var i = 0; i < textNodes.length; i++) {
        var node = textNodes[i];
        var words = []
        var regularExpression = /\w+/g;   //Metacharacters to do a global (g) search for word (w) characters within a string
        var match;

        //Search for a match in the nodes specified and update the 'words' array with a new length and position
        while ((match = regularExpression.exec(node.nodeValue)) != null) {
            words.push({
                length: match[0].length,
                position: match.index
            });
        }

        //Add all the words inside the 'wordsInTextNodes[] array'
        wordsInTextNodes[i] = words;
    };

    function messUpWords () {
        for (var i = 0; i < textNodes.length; i++) {
            var node = textNodes[i];

            for (var j = 0; j < wordsInTextNodes[i].length; j++) {
                // Only change a fifteenth of the words each round.
                if (Math.random() > 1/15)
                    continue;

                var wordMeta = wordsInTextNodes[i][j];

                var word = node.nodeValue.slice(wordMeta.position, wordMeta.position + wordMeta.length);
                var before = node.nodeValue.slice(0, wordMeta.position);
                var after  = node.nodeValue.slice(wordMeta.position + wordMeta.length);

                node.nodeValue = before + messUpWord(word) + after;
            };
        };
    }

    function messUpWord (word) {
        //Don't scrumble letters if the word is too short (3 characters)
        if (word.length < 3)
            return word;

        return word[0] + messUpMessyPart(word.slice(1, -1)) + word[word.length - 1];
    }

    function messUpMessyPart (messyPart) {
        if (messyPart.length < 2)
            return messyPart;

        var a, b;
        while (!(a < b)) {
            a = Math.floor(Math.random() * (messyPart.length - 1 - 0 + 1) + 0);   //Get random number between 0 (inclusive) and messyPart.length exclusive
            b = Math.floor(Math.random() * (messyPart.length - 1 - 0 + 1) + 0);   //
        }

        return messyPart.slice(0, a) + messyPart[b] + messyPart.slice(a+1, b) + messyPart[a] + messyPart.slice(b+1);
    }

    //Reset animation when pressing ESC
    $(document).keyup(function(e) {
        if (e.key === "Escape") {
            window.location.reload();
        }
    });

    setInterval(messUpWords, 150);
}

function setAsperger() {
    document.getElementById("my-alert").style.visibility = "visible";

    //Show alert message for few seconds before running the simulation
    setTimeout(function(){
        document.getElementById("my-alert").classList.add('alert-fade-out');  //Close alert with fade out animation
    }, 3000);

    setTimeout(() => {
        //Variables
        const h2 = "h2";
        const p = "p";
        const img = "img";
        const textEl_0 = ".text-element-0";
        const textEl_1 = ".text-element-1";
        const textEl_2 = ".text-element-2";
        const textEl_3 = ".text-element-3";
        const textEl_4 = ".text-element-4";
        const paragraphAnimation = "paragraph-animation";
        const headingAnimation = "heading-animation";
        const imgAnimation = "img-animation";
        const textAnimation_0 = "text-animation-0";
        const textAnimation_1 = "text-animation-1";
        const textAnimation_2 = "text-animation-2";
        const textAnimation_3 = "text-animation-3";
        const textAnimation_4 = "text-animation-4";

        (function() {
            function createElement(element, classname, textNode) {
                const el = document.createElement(element);
                el.setAttribute('class', classname);
                document.body.appendChild(el);
                if(textNode){
                    el.appendChild(document.createTextNode(textNode));
                }
            }

            createElement('div', 'img-element');

            function createTextElements(text, index) {
                createElement('span', `text-element-${index}`, text);
            }

            const texts = [
                'I must focus',
                'What time is it?',
                'I received a message on my phone',
                'Outside is a lovely day',
                'Should I answer that text message?',
            ];

            texts.forEach(createTextElements);

            function addClass(element, classname) {
                const el = document.querySelectorAll(element);

                for (let i = 0; i < el.length; i++)
                    el[i].classList.toggle(classname);
            }

            function removeClass(element, classname) {
                const el = document.querySelectorAll(element);

                for (let i = 0; i < el.length; i++)
                    el[i].classList.remove(classname);
            }

            //Loop animation
            function loopAnimations(){
                setTimeout(() => {
                    removeClass(textEl_4, textAnimation_4);
                    addClass(textEl_0, textAnimation_0);
                }, 0);

                setTimeout(() => {
                    addClass(p, paragraphAnimation);
                    addClass(img, imgAnimation);
                    addClass(h2, headingAnimation);
                }, 500);

                setTimeout(() => {
                    removeClass(textEl_0, textAnimation_0);
                    addClass(textEl_1, textAnimation_1);
                }, 5000);

                setTimeout(() => {
                    removeClass(textEl_1, textAnimation_1);
                    addClass(textEl_2, textAnimation_2);
                }, 10000);

                setTimeout(() => {
                    removeClass(textEl_2, textAnimation_2);
                    addClass(textEl_3, textAnimation_3);
                }, 15000);

                setTimeout(() => {
                    removeClass(p, paragraphAnimation);
                    removeClass(img, imgAnimation);
                    removeClass(h2, headingAnimation);
                    removeClass(textEl_3, textAnimation_3);
                    addClass(textEl_4, textAnimation_4);
                    loopAnimations();
                }, 20000);
            }
            loopAnimations();

        })();
    }, 3000);

    //Reset animation when pressing ESC
    $(document).keyup(function(e) {
        if (e.key === "Escape") {
            window.location.reload();
        }
    });
}

function closeAlert() {
    document.getElementById("my-alert").style.visibility='hidden';


}
