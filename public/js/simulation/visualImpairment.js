// var rngBlur = document.getElementById("rng-blur");
// var spnBlur = document.getElementById("spn-blur");
//
// rngBlur.oninput  = function() {
//     spnBlur.innerHTML = this.value;
//     document.getElementById("body").style.filter = "blur(2px)";
// }

function setHeavyBlur()
{
    document.getElementById("my-alert").style.visibility = "visible";

    //Show alert message for few seconds before running the simulation
    setTimeout(function(){
        document.getElementById("my-alert").classList.add('alert-fade-out');  //Close alert with fade out animation
    }, 3000);

    setTimeout(() => {
        document.getElementById("body").setAttribute("style","filter: blur(5px)");
        document.getElementById("body").setAttribute("style","-webkit-filter: blur(5px)");
    }, 4000);

    //Reset animation when pressing ESC
    $(document).keyup(function(e) {
        if (e.key === "Escape") {
            window.location.reload();
        }
    });
}

function setLightBlur()
{
    document.getElementById("my-alert").style.visibility = "visible";

    //Show alert message for few seconds before running the simulation
    setTimeout(function(){
        document.getElementById("my-alert").classList.add('alert-fade-out');  //Close alert with fade out animation
    }, 3000);

    setTimeout(() => {
        document.getElementById("body").setAttribute("style","filter: blur(1.8px) brightness(110%)");
        document.getElementById("body").setAttribute("style","-webkit-filter: blur(1.8px) brightness(110%)");
    }, 3500);

    //Reset animation when pressing ESC
    $(document).keyup(function(e) {
        if (e.key === "Escape") {
            window.location.reload();
        }
    });
}

function setColorblindness()
{
    const wrapper = document.getElementById("radio-sel");
    const filtered = document.querySelector('body');

    wrapper.addEventListener('click', (event) => {
        const target = event.target;

        if (target.matches('input')) {
            const value = target.value === 'none' ? 'none' : `url(#${target.value})`;
            filtered.style.filter = value;
        }
    });
}

function wrongColor()
{
    alert("Wrong!");
}

function rightColor()
{
    alert("CORRECT!");
}

//Set Central Vision Loss (CVL)
function setCVL()
{
    alert("Press 'ESC' on your keyboard to exit the simulation");

    $(document).ready(function() {
        $(document).on('mousemove', function(e) {
            $('#my-black-cursor').css({
                //Position cursor at the center of the "blurry" object
                left: e.pageX - 450,
                top: e.pageY - 450
            });
        })
    });

    //Reset animation when pressing ESC
    $(document).keyup(function(e) {
        if (e.key === "Escape") {
            window.location.reload();
        }
    });
}

//Set Peripheral Vision Loss (PVL)
function setPVL()
{
    alert("Press 'ESC' on your keyboard to exit the simulation");




    var cursorStyle = document.head.appendChild(document.createElement("style"));
    var htmlStyleBefore = document.head.appendChild(document.createElement("style"));

    //Assign style for the cursor
    cursorStyle.innerHTML = ":root {" +
        "cursor: none;" +
        "--cursorX: 50vw;" +
        "--cursorY: 50vh;";

    //Assign style to the html page around the cursor
    htmlStyleBefore.innerHTML = ":root:before {" +
        "content: '';" +
        "display: block;" +
        "width: 100%;" +
        "height: 100%;" +
        "position: fixed;" +
        "z-index: 1;" +
        "pointer-events: none;" +
        "background: radial-gradient(" +
        "    circle 20vmax at var(--cursorX) var(--cursorY)," +
        "    rgba(0,0,0,0) 0%," +
        "    rgba(0,0,0,1) 80%," +
        "    rgba(0,0,0,1) 100%" +
        ")" +
        "};";

    //Reset animation when pressing ESC
    $(document).keyup(function(e) {
        if (e.key === "Escape") {
            window.location.reload();
        }
    });
}
