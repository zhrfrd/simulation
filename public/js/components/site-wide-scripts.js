// site wide scripts

$(document).ready(function () {
    if ($(".card-links").length > 0) {
        $(".card-links ul").addClass("fa-ul");
        $(".card-links ul li").each(function () {
            $(this).prepend("<i class=\"fa fa-li fa-arrow-right\"></i>");
        });
    }
    if ($(".body-text a").length > 0) {
        $(".body-text a").each(function () {
            var address = $(this).attr("href");
            if (address) {
                if (address.substr(address.length - 4) == ".pdf" || address.substr(address.length - 4) == ".PDF") {
                    var content = $(this).html();
                    $(this).html(content + " <i class=\"fal fa-file-pdf\"></i>");
                } else if (address.substr(address.length - 4) == ".doc" || address.substr(address.length - 4) == ".DOC" || address.substr(address.length - 5) == ".docx" || address.substr(address.length - 5) == ".DOCX") {
                    var content = $(this).html();
                    $(this).html(content + " <i class=\"fal fa-file-word\"></i>");
                } else if (address.substr(address.length - 4) == ".ppt" || address.substr(address.length - 4) == ".PPT") {
                    var content = $(this).html();
                    $(this).html(content + " <i class=\"fal fa-file-powerpoint\"></i>");
                }
                var content = $(this).html();
                if (content.indexOf("[BROKEN]") !== -1) {
                    $(this).css({color: "#e52828", textDecoration: "underline", fontWeight: "900"});
                }
            }
        });
    }
    if ($(".search-bar").length > 0) {
        $(".search-bar input.text").addClass("form-control").attr("placeholder", "Search").attr("value", "");
        $(".search-bar .action").addClass("btn").addClass("btn-outline-success");
    }
//        // accordion arrows
//        if ($('.accordion').length>0)
//        	{
//	        	console.log('gg');
//	        	$('.accordion .card-header .btn[aria-expanded="false"]').prepend('<i class="fal fa-angle-right"></i> ');
//	        	$('.accordion .card-header .btn[aria-expanded="true"]').prepend('<i class="fal fa-angle-down"></i> ');
//	        	$('.accordion .card-header').on('click', '.btn', function(){
//					console.log('gggfd');
////					$('.accordion i.fal').removeClass('fa-angle-down').addClass('fa-angle-right');
//					var icon = $(this).find('.fal');
//					console.log(icon);
//					$(icon).toggleClass('fa-angle-right fa-angle-down');
//				});
//			}

    // add click to top dropdown link
    if ($("li.dropdown").length > 0) {
        $("li.dropdown").on("click", function () {
            var $el = $(this);
            if ($el.hasClass("show")) {
                var $a = $el.children("a.dropdown-toggle");
                if ($a.length && $a.attr("href")) {
                    location.href = $a.attr("href");
                }
            }
        });
    }

});
