// accordion scripts

$(document).ready(function () {
    if ($(".accordion").length > 0) {
        $(".accordion button").click(function () {
            $(this).parent().parent().parent().parent().find("button .box-icon svg").removeClass("fas").addClass("fal");
            $(this).find(".box-icon svg").removeClass("fal").addClass("fas");
        });
    }
});
