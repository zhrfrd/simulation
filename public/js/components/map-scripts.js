// homepage scripts

$(document).ready(function () {
    if ($(".region-list").length > 0) {
        $(".region-list dt a").mouseenter(function () {
            var region = $(this).html();
            console.log(region);
            $(".map-regions").find("[data-region=\"" + region + "\"]").addClass("hovered");
        });
        $(".region-list dt a").mouseleave(function () {
            var region = $(this).html();
            $(".map-regions").find("[data-region=\"" + region + "\"]").removeClass("hovered");
        });
    }

});
