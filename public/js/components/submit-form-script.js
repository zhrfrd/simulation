// submit form scripts
// Based on script https://riptutorial.com/silverstripe/example/16420/creating-a-simple-ajax-form
$(document).ready(function () {
    $(".js-ajax-form").on("submit", function (e) {
        e.preventDefault();
        $("#Form_Form_action_submit").prop("disabled", true);
        var $form = $(this);
        var formData = $form.serialize();
        var formAction = $form.prop("action");
        var formMethod = $form.prop("method");
        var encType = $form.prop("enctype");
        var message = $("#Form_Form_Message").val();
        var name = $("#Form_Form_Name").val();
        var email = $("#Form_Form_Email").val();

        if (!message || !name || !email) {
            if (isWelsh()) {
                displayError("Mae angen pob maes.");
            } else {
                displayError("All fields are required.");
            }
            $("#Form_Form_action_submit").prop("disabled", false);
            return;
        }

        if (message.includes("http://") || message.includes("https://")) {
            if (isWelsh()) {
                displayError("Yn anffodus ni ellid danfon eich neges gan ei bod yn cynnwys dolen, ceisiwch eto.");
            } else {
                displayError("Unfortunately your message couldn't be sent as it contains a link, please try again.");
            }
            $("#Form_Form_action_submit").prop("disabled", false);
            return;
        }

        $.ajax({
            beforeSend: function (jqXHR, settings) {
                if ($form.prop("isSending")) {
                    return false;
                }
                $form.prop("isSending", true);
            },
            complete: function (jqXHR, textStatus) {
                $form.prop("isSending", false);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                window.location = window.location;
                displayError("there was an error! Please try again.");
                $("#Form_Form_action_submit").prop("disabled", false);
                $form.prop("isSending", false);
            },
            success: function (data, textStatus, jqXHR) {
                var $holder = $form.parent();
                $holder.fadeOut("normal", function () {
                    $holder.html(data).fadeIn();
                });
            },

            contentType: encType,
            data: formData,
            type: formMethod,
            url: formAction
        });

        function isWelsh()
        {
            return location.href.includes("/cy/");
        }

        function displayError(error)
        {
            var message = "<div class=\"alert alert-warning alert-dismissible\" role=\"alert\">\n" +
                error +
                "    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n" +
                "        <span aria-hidden=\"true\">&times;</span>\n" +
                "    </button>\n" +
                "</div>";

            $("#Form_Form_error").html(message);
            $("#Form_Form_error:hidden").show("slow");
        }
    });
});
