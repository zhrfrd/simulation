// cookie scripts

$(document).ready(function () {

    // ==== set variables
    var siteAnalyticsCookie = googleAnalyticsSite + "-analytics-cookie";
    var siteAcknowledge = googleAnalyticsSite + "-acknowledge";
    var googleAnalytics = "ga-disable-" + googleAnalyticsCode;

    // set cookie
    function setCookie(cname, cvalue, exdays)
    {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    // get cookie
    function getCookie(cname)
    {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(";");
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == " ") {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    // check for cookie switch
    if ($(".analytics-switch").length > 0) {
        // Check language
        var yesText = "On";
        var noText = "Off";
        if (window.location.href.indexOf("/cy/") > -1) {
            yesText = "Ar";
            noText = "I ffwrdd";
        }

        // check for cookie on load
        if (getCookie(siteAnalyticsCookie) == "on") {
            $(".analytics-switch").prop("checked", true);
            $(".cookie-status").removeClass("text-danger").addClass("text-success").html(yesText);
        } else {
            $(".analytics-switch").prop("checked", false);
            $(".cookie-status").removeClass("text-success").addClass("text-danger").html(noText);
        }

        // get click and set cookie according to value
        $(".analytics-switch").click(function () {
            if ($(this).is(":checked")) {
                $(".cookie-status").removeClass("text-danger").addClass("text-success").html(yesText);
                setCookie(siteAnalyticsCookie, "on", "300");
                window[googleAnalytics] = false;
            } else {
                $(".cookie-status").removeClass("text-success").addClass("text-danger").html(noText);
                setCookie(siteAnalyticsCookie, "off", "300");
                window[googleAnalytics] = true;
            }
        });
    }


    // notifications
    var ack = getCookie(siteAcknowledge);

    if (!ack) {
        $(".site-notification").animate({bottom: "0%", display: "block"}, 1000);
        $(".site-overlay").css("display", "block");
        $("#accept-cookies-button").trigger("focus");
    }

    $(".site-notification-acknowledge").click(function () {
        setCookie(siteAcknowledge, "on", "1");
        $(".site-notification").animate({bottom: "-100%", display: "none"}, 1000);
        $(".site-overlay").css("display", "none");
        setCookie(siteAnalyticsCookie, "on", "300");
    });

    $(".site-notification-settings").click(function () {
        setCookie(siteAcknowledge, "on", "1");
        $(".site-notification").animate({bottom: "-100%", display: "none"}, 1000);
        $(".site-overlay").css("display", "none");
    });

});
