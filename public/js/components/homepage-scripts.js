// homepage scripts

$(document).ready(function () {
    // slider

    $(".main-slider").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 6000,
        arrows: true,
        dots: true,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: "linear",
        prevArrow: "<button type=\"button\" class=\"slick-prev\"><</button>",
        nextArrow: "<button type=\"button\" class=\"slick-next\">></button>"
    });

});
