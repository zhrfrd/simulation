# HEIW base site SS 4.7

## Overview

Created from SilverStripe ([silverstripe.org](http://silverstripe.org)) installation.

This base site has been configured as a starting point for new Silverstripe projects.

## Requirements

- PHP 7.1+
  - composer
- MySQL
- Git

## Installation

To create a new Repo from this one follow these steps:

- Create new/empty repository on GitLab
- Clone the repo down onto your local machine `git clone <project>`
- Copy files into it from this **HEIW base site** repo (download the zip)
  - Note: if **HEIW base site** is cloned do not to copy the `.git` folder, leave the one already there in the empty repo

Back in the new project repo, dir which has been pulled and **HEIW base site** copied into:

- Run `composer update` on new local repo
- Create `.env` file with following contents `copy .env.example .env` (or `cp  .env.example .env`)
    - Paste in the template below
    - Update `<project>` and `<db_password>` to your new projects name and local db admin password
        - Tip: search and replace `<project>` with the new project name to update this README.md 

```ini
SS_BASE_URL="http://<project>.local:8888/"

SS_DATABASE_CLASS="MySQLPDODatabase"
SS_DATABASE_NAME="<project>"
SS_DATABASE_PASSWORD="<db_password>"
SS_DATABASE_SERVER="localhost"
SS_DATABASE_USERNAME="root"

SS_ENVIRONMENT_TYPE="dev"

SS_DEFAULT_ADMIN_USERNAME="admin@example.com"
SS_DEFAULT_ADMIN_PASSWORD="password"
```

- Visit the site in browser and run `http://<project>.local:8888/dev/build?flush=all`

## Create an admin account

Access the admin area:

- Visits the site `http://<project>.local:8888/admin`
- Click the Security menu (left)
- Click Add Member, create/update the admin account
- Remove `SS_DEFAULT_ADMIN_USERNAME` and `SS_DEFAULT_ADMIN_PASSWORD` from the `.env` file

## Language

The Silverstripe fluent module has been pre-installed. This module will allow pages to be created in English and Welsh.

### Configure Locals

- sign in to the admin portal of the CMS, `http://<project>.local:8888/admin`
- from the left-hand menu select **Locales** (globe icon)
- **Locales** will open
  - click **Add Locale**
    - Select **English (United Kingdom)** from the drop down list (should be the default)
    - **Title** it is important to change to only **English** - remove (United Kingdom)
    - **URL** Segment - **en**
    - [x] This is the global default locale
    - **Domain** (none)
    - **+ Create**
    - Click Locales < arrow to return to locals menu. English should display.
  - Repeat for Welsh:
    - start typing _Welsh_ and select **Welsh (United Kingdom)** from the drop down list.
    - **Title** it is important to change to only **Welsh** - remove (United Kingdom)
    - **URL** Segment - **cy**
    - [ ] This is the global default locale (uncheck)
    - **Domain** (none)
    - **+ Create**

The Titles **English** and **Welsh** are used to identify the locales, for example, incorrect titles will not display 
the flags in the heading.

Navigate to Pages and publish every page in English, then every page in Welsh, updating the title and text as required.

## Prepros

Prepros should be configured to start in the **public** folder of the project:

- click **+ Add Project**
- navigate to the project's public folder
- click Add project
- under PROJECTS, right click the new **public** project
- select **Project Settings**
- change the name for the new project's name
- click X to close.

### `heiw-scripts.js`

Start Prepros and make sure `heiw-scripts.js` is configured to **Process Automatically**:

Output file: `/js/min/heiw-scripts.min.js`

File Options
- [x] Process Automatically
- [x] Create a Source Map

CONCAT-JS:
- [x] Join @append/@prepend

BABEL
- [ ] Transpile with Babel

 BUNDLE-JS
 - [ ] Bundle Imports and Requires

 Minify-JS
 - [x] Minify JS
 - [ ] Mangle top Level Identifiers

 UPLOAD/EXPORT
 - [x] Include File while Uploading

When `heiw-scripts.js` is saved Prepros should automatically compile

### `style.less`

Next using Prepros navigate to `less/style.less` and check it is configured to **Process Automatically**:

Output file: `/css/style.css`

File Options
- [x] Process Automatically
- [x] Create a Source Map

LESS:
- [ ] Line Numbers

AUTOPREFIXER
- [x] Prefix CSS

Minify-CSS
 - [ ] Minify CSS

 UPLOAD/EXPORT
 - [ ] Include File while Uploading

Click **Process File**

When any `.less` are saved, Prepros should automatically compile.

### Update

Open `style.less` and update the header text:

```less
/*
Theme Name: HEIW Base site
Theme URI: http://heiw-base.local:8888
Description: HTML5/CSS3 bootstrap responsive theme for HEIW
Version: 1.0
Author: Gareth J Peters
*/
```

Change `HEIW Base site`, `heiw-base` and `HEIW` for the new site/project name and url

Prepros should automatically process, a notification will display in the bottom right corner.

## Verify

Open `/js/min/heiw-scripts.min.js`

It should be two lines, one lone line starting `$(document).ready((function(){$((function(){$('[data-toggle="popover"]').`
 with another line `//# sourceMappingURL=heiw-scripts.min.js.map`

Open `/css/style.css`

The css should be human-readable (not minified), starting with the name of the project, amended above, if it is empty or
 still has .

## Troubleshooting

Check **heiw-scripts.js** file and amend:

```js
// This is for codekit and prepros
// @codekit-prepend "components/site-wide-scripts.js"
// @codekit-prepend "components/submit-form-script.js"
// @codekit-prepend "components/flip-scripts.js"
// @codekit-prepend "components/accordion-scripts.js"
// @codekit-prepend "components/homepage-scripts.js"
// @codekit-prepend "components/map-scripts.js"
// @codekit-prepend "components/cookie-scripts.js"
```

To:

```js
// This is for codekit
// @codekit-prepend "components/site-wide-scripts.js"
// @codekit-prepend "components/flip-scripts.js"
// @codekit-prepend "components/accordion-scripts.js"
// @codekit-prepend "components/homepage-scripts.js"
// @codekit-prepend "components/map-scripts.js"
// @codekit-prepend "components/cookie-scripts.js"

// This is for prepros
// @prepros-prepend "components/site-wide-scripts.js"
// @prepros-prepend "components/submit-form-script.js"
// @prepros-prepend "components/flip-scripts.js"
// @prepros-prepend "components/accordion-scripts.js"
// @prepros-prepend "components/homepage-scripts.js"
// @prepros-prepend "components/map-scripts.js"
// @prepros-prepend "components/cookie-scripts.js"
```

If Prepros goes into an infinite build (the spinner never stops), empty the **heiw-scripts.js**, then re-complete with 
Prepros (you may need to close and re-open Prepos to exit the build), once the original build is empty add the 
`@codekit-prepend` ... lines, and it will build properly.

## Google Analytics

Click **Settings** and select the **Analytics** tab. 

- **Google Analytics site** should be the name of the project, which is used for the name of the cookie, this should be 
  lowercase kebab format.
- **Google Analytics code** is the code from [Google Analtics](https://analytics.google.com/).

JavaScript does not need to be re-processed.

## Contact form

The contact form has two parts, the controller **ContactPageController.php** and the script **submit-form-script.js**, 
which handles the ajax call using jQuery. If any form fields are changed in the controller, the script will also need to 
be updated with the required fields. 
