<?php

namespace {

    use SilverStripe\CMS\Controllers\ContentController;
    use SilverStripe\View\Requirements;

    class PageController extends ContentController
    {
        /**
         * An array of actions that can be accessed via a request. Each array element should be an action name, and the
         * permissions or conditions required to allow the user to access it.
         *
         * <code>
         * [
         *     'action', // anyone can access this action
         *     'action' => true, // same as above
         *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
         *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
         * ];
         * </code>
         *
         * @var array
         */
        private static $allowed_actions = [];

        protected function init()
        {
            parent::init();
            // You can include any CSS or JS required by your project here.
            // See: https://docs.silverstripe.org/en/developer_guides/templates/requirements/
            Requirements::css("https://fonts.googleapis.com/css?family=Roboto:300,300i,400,700,700i");
            //Requirements::css("https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css");
            Requirements::themedcss("/css/bootstrap-4.min.css");
            Requirements::themedcss("/slick/slick.css");
            Requirements::themedcss("/slick/slick-theme.css");
//          Requirements::themedcss("/css/all.min.css");
            Requirements::themedcss("/css/style.css");

            // block old version of jQuery which is used in CMS - flagged in pen-test
            //Requirements::block(SAPPHIRE_DIR .'/thirdparty/jquery/jquery.js');

            // add latest version for use in website
            Requirements::javascript("https://code.jquery.com/jquery-3.5.1.min.js");
            Requirements::themedjavascript("/js/min/bootstrap-4.min.js");
            Requirements::themedjavascript("/js/min/popper.min.js");
            Requirements::themedjavascript("/js/min/all.min.js");
            Requirements::themedjavascript("/slick/slick.min.js");
            Requirements::themedjavascript("/js/min/heiw-scripts.min.js");
        }


        // get page teasers
        public function GetCustomTeasers($IDs)
        {
            $IDs = explode(",", $IDs);
            print_r($IDs);
            $sliders = Page::get()
                ->filter(array(
                    'ID'    =>  $IDs
                ));

            return $sliders;
        }

        // get URL
        public function url()
        {
            if (isset($_SERVER['HTTPS'])) {
                $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
            } else {
                $protocol = 'http';
            }
            return $protocol . "://" . $_SERVER['HTTP_HOST'];
        }

        // get full URL
        public function fullurl()
        {
            if (isset($_SERVER['HTTPS'])) {
                $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
            } else {
                $protocol = 'http';
            }
            return $protocol . "://" . $_SERVER['HTTP_HOST'] . '/' . $_SERVER['REQUEST_URI'];
        }

        // language checker
        public function CheckLanguage()
        {
            $url = self::fullurl();
            if (strpos($url, '/cy/') !== false) {
                return 'welsh';
            } else {
                return 'english';
            }
        }

        // admin links
        public function AdminLink()
        {
            $url = self::url();
            return $url . '/admin/pages';
        }
        public function EditPageLink()
        {
            $url = self::url();
            return $url . '/admin/pages/edit/show/' . $this->ID;
        }
    }
}
