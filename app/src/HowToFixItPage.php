<?php

namespace {

    use SilverStripe\CMS\Model\SiteTree;
    use SilverStripe\Assets\Image;
    use SilverStripe\AssetAdmin\Forms\UploadField;
    use SilverStripe\Forms\TextField;
    use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
    use SilverStripe\Forms\TextareaField;

    class HowToFixItPage extends Page
    {
        //Get full URL
        public function fullurl() {
            if(isset($_SERVER['HTTPS']))
                $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";

            else
                $protocol = 'http';

            return $protocol . "://" . $_SERVER['HTTP_HOST'] . '/' . $_SERVER['REQUEST_URI'];
        }

        //Retrieve subsection name from url
        public function getSubsectionName()
        {
            $url = self::fullurl();   //Get full url address

            if (strpos($url, '/how-to-fix-it/images/') !== false)
                return 'images';

            else if (strpos($url, '/how-to-fix-it/headings/') !== false)
                return 'headings';

            else if (strpos($url, '/how-to-fix-it/colors/') !== false)
                return 'colors';

            else if (strpos($url, '/how-to-fix-it/links/') !== false)
                return 'links';
        }
    }
}
