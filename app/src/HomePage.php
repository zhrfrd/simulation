<?php

namespace {

    use SilverStripe\CMS\Model\SiteTree;
    use SilverStripe\Assets\Image;
    use SilverStripe\AssetAdmin\Forms\UploadField;
    use SilverStripe\Forms\TextField;
    use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
    use SilverStripe\Forms\TextareaField;

    class HomePage extends Page
    {
        private static $db = [
            'BannerText'        =>  'Varchar',
            'LeftBoxTitle'      =>  'Varchar',
            'LeftBoxContent'    =>  'HTMLText',
            'RightBoxTitle'     =>  'Varchar',
            'RightBoxContent'   =>  'HTMLText',
            'VideoIframe'       =>  'Varchar'
        ];

        private static $has_one = [
            'LeftBoxImage'  =>  Image::class,
            'RightBoxImage' =>  Image::class
        ];

        private static $owns = [
            'LeftBoxImage',
            'RightBoxImage'
        ];

        public function getCMSFields()
        {
            $fields = parent::getCMSFields();
            $fields->addFieldToTab('Root.Main', TextareaField::create('BannerText', 'Banner text'), 'LeadParagraph');
            $fields->addFieldToTab(
                'Root.Main',
                TextField::create(
                    'VideoIframe',
                    'Video embed code (just add the video code from the URL, the part after "watch?v=")'
                ),
                'LeadParagraph'
            );
            // ...
            $fields->addFieldToTab('Root.Left box', TextField::create('LeftBoxTitle', 'Left box title'));
            $fields->addFieldToTab('Root.Left box', HTMLEditorField::create('LeftBoxContent', 'Left box content'));
            $fields->addFieldToTab(
                'Root.Left box',
                $leftbox_image1 = UploadField::create('LeftBoxImage', 'Left box image')
            );
            // ...
            $fields->addFieldToTab('Root.Right box', TextField::create('RightBoxTitle', 'Right box title'));
            $fields->addFieldToTab('Root.Right box', HTMLEditorField::create('RightBoxContent', 'Right box content'));
            $fields->addFieldToTab(
                'Root.Right box',
                $rightbox_image2 = UploadField::create('RightBoxImage', 'Right box image')
            );
            // ...
            $leftbox_image1->getValidator()->setAllowedExtensions(['jpg', 'jpeg', 'png']);
            $rightbox_image2->getValidator()->setAllowedExtensions(['jpg', 'jpeg', 'png']);

            return $fields;
        }
    }
}
