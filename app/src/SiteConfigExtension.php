<?php

use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;

class SiteConfigExtension extends DataExtension
{
    private static $db = [
        'TeamName'              =>  'Varchar(255)',
        'MainAddress'           =>  'HTMLText',
        'MainEmail'             =>  'Varchar(255)',
        'MainTelephone'         =>  'Varchar(255)',
        'FacebookLink'          =>  'Varchar(255)',
        'TwitterLink'           =>  'Varchar(255)',
        'InstagramLink'         =>  'Varchar(255)',
        'LinkedinLink'          =>  'Varchar(255)',
        'YouTubeLink'           =>  'Varchar(255)',
        'SystemStatus'          =>  'HTMLText',
        'GoogleAnalyticsSite'   =>  'Varchar(255)',
        'GoogleAnalyticsCode'   =>  'Varchar(255)',
        'NotificationDisplay'   =>  'Boolean',
        'NotificationOverlay'   =>  'Boolean',
        'NotificationText'      =>  'HTMLText',
        'NotificationButton'    =>  'Varchar',

    ];

    private static $has_one = [
        'MastheadImage'     =>  Image::class
    ];

    private static $owns = [
        'MastheadImage'
    ];

    public function updateCMSFields(FieldList $fields)
    {
        $fields->addFieldsToTab(
            "Root.Main",
            [
                TextField::create("TeamName", "Team name"),
                HTMLEditorField::create("MainAddress", "Main address"),
                TextField::create("MainEmail", "Main email - also used for contact form"),
                TextField::create("MainTelephone", "Main telephone"),
                $site_logo = UploadField::create('MastheadImage', 'Masthead image'),
                HTMLEditorField::create("SystemStatus", "System status"),
            ]
        );
        // ==
        $fields->addFieldsToTab(
            "Root.Social media",
            [
                TextField::create("FacebookLink", "Facebook link"),
                TextField::create("TwitterLink", "Twitter link"),
                TextField::create("InstagramLink", "Instagram link"),
                TextField::create("LinkedinLink", "Linkedin link"),
                TextField::create("YouTubeLink", "YouTube link"),
            ]
        );
        // ==
        $fields->addFieldsToTab(
            "Root.Analytics",
            [
                TextField::create('GoogleAnalyticsSite', "Google Analytics site"),
                TextField::create('GoogleAnalyticsCode', "Google Analytics code"),
            ]
        );
        // ==
        $fields->addFieldsToTab(
            'Root.Notification',
            [
                CheckboxField::create('NotificationDisplay', 'Display Notification?'),
                CheckboxField::create(
                    'NotificationOverlay',
                    'Provide Overlay? (Users MUST acknowledge before using the site)'
                ),
                HTMLEditorField::create("NotificationText", "Notification Text"),
                TextField::create("NotificationButton", "Acknowledge Button Text"),
            ]
        );

        $site_logo->getValidator()->setAllowedExtensions(['jpg', 'jpeg', 'png']);

        return $fields;
    }
}
