<?php

namespace {

    use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

    class CookiePage extends Page
    {
        private static $db = [
            'AnalyticsText' =>  'HTMLText',
            'EssentialText' =>  'HTMLText',
            'OtherText'     =>  'HTMLText'
        ];

        private static $has_one = [];

        public function getCMSFields()
        {
            $fields = parent::getCMSFields();
            // ...
            $fields->addFieldToTab(
                'Root.Main',
                HTMLEditorField::create('AnalyticsText', 'Text about analytics cookies')
            );
            $fields->addFieldToTab(
                'Root.Main',
                HTMLEditorField::create('EssentialText', 'Text about essential cookies')
            );
            $fields->addFieldToTab('Root.Main', HTMLEditorField::create('OtherText', 'Summary text about cookies'));
            // ...
            $fields->removeFieldFromTab("Root.Main", "MainImage");
            $fields->removeFieldFromTab("Root.Main", "ShowTeasers");
            $fields->removeFieldFromTab("Root.Main", "TeaserIDs");
            // ...
            return $fields;
        }
    }
}
