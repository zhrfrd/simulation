<?php /** @noinspection PhpUnusedPrivateFieldInspection */

// form
use SilverStripe\Control\Director;
use SilverStripe\Forms\Form;
use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\EmailField;
use SilverStripe\Forms\TextareaField;

// emailer
use SilverStripe\Control\Email\Email;

class ContactPageController extends PageController
{
    private static $allowed_actions = [
        'Form'
    ];

    public function Form()
    {
        return (
        Form::create(
            $this,
            'Form',
            $this->fields(),
            $this->actions(),
            $this->validator()
        )
        )
            ->addExtraClass('js-ajax-form')
            ->setLegend('Contact form');
    }

    public function submit($data, $form)
    {
        if ($this->doesMessageContainLink($data)) {
            $badMessage = ($this->CheckLanguage() === 'welsh')
                ? 'Yn anffodus ni ellid danfon eich neges gan ei bod yn cynnwys dolen, ceisiwch eto.'
                : "Unfortunately your message couldn't be sent as it contains a link, please try again.";
            $form->sessionMessage($badMessage, 'bad');
            return $this->redirectBack();
        }

        $email = new Email();

        $email->setTo('heiw.webforms@wales.nhs.uk');
//        $email->setTo('gareth.peters@wales.nhs.uk');
//        $email->setTo('michael.pritchard3@wales.nhs.uk');
//          $email->setTo('heiw.DEPARTMENT@wales.nhs.uk');

//          $email->setBCC('heiw.webforms@wales.nhs.uk');
//          $email->setBCC('gareth.peters@wales.nhs.uk');
//          $email->setBCC('michael.pritchard3@wales.nhs.uk');
        $email->setFrom('donotreply@SITE.heiw.wales');
        $email->setSubject("Contact Message from {$data["Name"]}");


        $messageBody = "
    <p>You have received a message from the *SITE* website, please see the details below:</p>
  <hr />
  <p><strong>Name:</strong> {$data['Name']}</p>
  <p><strong>Email:</strong> {$data['Email']}</p>

  <p><strong>Message:</strong> {$data['Message']}</p>
  ";
        $email->setBody($messageBody);
        $email->send();

        if ($this->CheckLanguage() === 'welsh') {
            $thankyouMessage = 'Diolch am eich ymholiad am *SITE*. Byddwn yn cysylltu yn ôl yn fuan.';
        } else {
            $thankyouMessage = 'Thank you for your enquiry about *SITE*. We’ll come back to you shortly.';
        }

        $data = [
            'LeadParagraph' => $thankyouMessage,
            'Form' => ''
        ];

        if (Director::is_ajax()) {
            return $this->customise($data)->renderWith('Includes/Contact_complete');
        }

        return $data;
    }

    private function fields()
    {
        if ($this->CheckLanguage() === 'welsh') {
            $nameLabel = 'Eich enw (angenrheidiol)';
            $emailLabel = 'Eich ebost (angenrheidiol)';
            $messageLabel = 'Eich neges (angenrheidiol)';
        } else {
            $nameLabel = 'Your name (required)';
            $emailLabel = 'Your email (required)';
            $messageLabel = 'Your message (required)';
        }

        return new FieldList(
            new TextField('Name', $nameLabel),
            new EmailField('Email', $emailLabel),
            new TextareaField('Message', $messageLabel)
        );
    }

    private function actions()
    {
        $button_label = ($this->CheckLanguage() === 'welsh') ? 'Anfon neges' : 'Send message';
        $button = new FormAction('submit', $button_label);
        $button->removeExtraClass('btn-primary')->addExtraClass('btn-navy');
        return new FieldList($button);
    }

    private function validator()
    {
        return new RequiredFields([
            'Name',
            'Email',
            'Message'
        ]);
    }

    private function doesMessageContainLink($data): bool
    {
        return strpos($data['Message'], 'http://') !== false
            || strpos($data['Message'], 'https://') !== false;
    }
}
