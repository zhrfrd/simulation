<?php

namespace {

    use SilverStripe\CMS\Model\SiteTree;
    use SilverStripe\Assets\Image;
    use SilverStripe\AssetAdmin\Forms\UploadField;
    use SilverStripe\Forms\TextField;
    use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
    use SilverStripe\Forms\OptionsetField;
    use SilverStripe\Forms\TextareaField;
    use SilverStripe\Forms\NumericField;
    use SilverStripe\Forms\ListBoxField;

    class Page extends SiteTree
    {
        private static $db = [
            'LeadParagraph' =>  'Text',
//          'Teaser'        =>  'Boolean',
//          'Slider'        =>  'Boolean',
//          'SliderContent' =>  'Text',
//          'SliderOrder'   =>  'Int',
            'TeaserText'    =>  'Text',
            'ShowTeasers'   =>  'Boolean',
            'TeaserIDs'     =>  'Text'
        ];

        private static $has_one = [
            'MainImage'     =>  Image::class,
            'TeaserImage'   =>  Image::class
//          'SliderImage'   =>  Image::class
        ];

//      private static $has_many = [
//          'Teasers'   =>  SiteTree::class
//      ];

        private static $owns = [
            'MainImage',
            'TeaserImage'
//          'SliderImage'
        ];

        public function getCMSFields()
        {
            $fields = parent::getCMSFields();
            // ...
            $fields->addFieldToTab('Root.Main', TextareaField::create('LeadParagraph', 'Lead paragraph'), 'Content');
            $fields->addFieldToTab('Root.Main', $image = UploadField::create('MainImage', 'Main image'));
            $fields->addFieldToTab(
                'Root.Main',
                OptionsetField::create('ShowTeasers', 'Do you want to display teasers on this page?', array(
                '1' => 'Yes',
                '0' => 'No'
                ))
            );
            $fields->addFieldToTab(
                'Root.Main',
                TextareaField::create('TeaserIDs', 'Page IDs for teasers (separate with a comma)')
            );
//          $fields->addFieldToTab(
//              'Root.Main',
//              ListBoxField::create('Teasers', 'Which pages would you like as a teaser(s)', SiteTree::get()
//                  ->sort('Title', 'ASC')
//                  ->map('ID', 'Title')
//                  ->toArray()
//              ));
            // ..
//          $fields->addFieldToTab(
//              'Root.Teaser',
//              OptionsetField::create('Teaser', 'Do you want this page to appear as a homepage teaser?', array(
//              '1' => 'Yes',
//              '0' => 'No'
//          )));
            $fields->addFieldToTab('Root.Teaser', TextareaField::create('TeaserText', 'Teaser text (if applicable)'));
            $fields->addFieldToTab('Root.Teaser', $teaser_image = UploadField::create('TeaserImage', 'Teaser image'));
            // ..
//          $fields->addFieldToTab(
//              'Root.Slider',
//              OptionsetField::create('Slider', 'Do you want this page to appear in the homepage slider?', array(
//              '1' => 'Yes',
//              '0' => 'No'
//          )));
//          $fields->addFieldToTab('Root.Slider', TextareaField::create('SliderContent', 'Slider content'));
//          $fields->addFieldToTab(
//              'Root.Slider',
//               NumericField::create(
//                  'SliderOrder',
//                  'Slider order (Please enter a numeric value to order the slides, lower numbers will be first)'
//              )
//          );
//          $fields->addFieldToTab('Root.Slider', $slider_image = UploadField::create('SliderImage', 'Slider image'));

            $image->getValidator()->setAllowedExtensions(['jpg', 'jpeg', 'png']);
            $teaser_image->getValidator()->setAllowedExtensions(['jpg', 'jpeg', 'png']);
//          $slider_image->getValidator()->setAllowedExtensions(['jpg', 'jpeg', 'png']);

            return $fields;
        }
    }
}
