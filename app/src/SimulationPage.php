<?php

namespace {

    use SilverStripe\CMS\Model\SiteTree;
    use SilverStripe\Assets\Image;
    use SilverStripe\AssetAdmin\Forms\UploadField;
    use SilverStripe\Forms\TextField;
    use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
    use SilverStripe\Forms\TextareaField;

    class SimulationPage extends Page
    {
        //Get full URL
        public function fullurl() {
            if(isset($_SERVER['HTTPS']))
                $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";

            else
                $protocol = 'http';

            return $protocol . "://" . $_SERVER['HTTP_HOST'] . '/' . $_SERVER['REQUEST_URI'];
        }

        //Retrieve subsection name from url
        public function getSubsectionName()
        {
            $url = self::fullurl();   //Get full url address

            if (strpos($url, '/simulation/visual-impairment/') !== false)
                return 'visual-impairment';

            else if (strpos($url, '/simulation/mobility-issues/') !== false)
                return 'mobility-issues';

            else if (strpos($url, '/simulation/auditory-impairment/') !== false)
                return 'auditory-impairment';

            else if (strpos($url, '/simulation/cognitive-disability/') !== false)
                return 'cognitive-disability';
        }
    }
}
