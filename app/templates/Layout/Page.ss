<section class="masthead"
         style="background-image: linear-gradient(0deg, rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.2)), <% with $SiteConfig.MastheadImage %>url($Link)<% end_with %>;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="page-subtitle">$SiteConfig.Title</div>
                <h1 class="page-title align-self-end">$Title</h1>
            </div>
        </div>
    </div>
</section>
<section class="main-page">
    <% if $SiteConfig.SystemStatus %>
        <% include SiteStatus %>
    <% end_if %>
    <div class="container">
        <div class="row">
            <div class="<% if $Children %>col-sm-9<% else_if $Parent %>col-sm-9<% else %>col-sm-12<% end_if %> order-sm-last">
                <% if $MainImage %>
                    <figure class="image main-image placeholder">
                        <% with $MainImage %>
                            <img src="$Link" alt="$Title" class="float-right"/>
                        <% end_with %>
                    </figure>
                <% end_if %>
                <% if $LeadParagraph %>
                    <p class="lead">$LeadParagraph</p>
                <% end_if %>
                <% if $Content %>
                    $Content
                    <div class="mb-4"></div>
                <% end_if %>
                $Form
                <% if $ShowTeasers %>

                    <% if $GetCustomTeasers($TeaserIDs) %>

                        <hr/>

                        <div class="row child-teasers">

                            <% loop $GetCustomTeasers($TeaserIDs) %>

                                <div class="col-4">
                                    <div class="card">
                                        <a href="$Link">
                                            <% if $TeaserImage %>
                                                <% with $TeaserImage.Fill(200,135) %>
                                                    <img src="$Link" alt="teaser image" class="card-img-top"/>
                                                <% end_with %>
                                            <% else_if $MainImage %>
                                                <% with $MainImage.Fill(200,135) %>
                                                    <img src="$Link" alt="main image" class="card-img-top"/>
                                                <% end_with %>
                                            <% end_if %>
                                        </a>
                                        <div class="card-body">
                                            <h5 class="card-title"><a href="$Link">$Title</a></h5>
                                            <p class="card-text">$Aims.FirstSentence</p>
                                        </div>
                                    </div>
                                </div>



                            <% end_loop %>

                        </div>

                    <% end_if %>

                <% else_if $Children %>

                    <div class="row child-teasers">
                        <div class="col-sm-12">
                            <h4><% if $CheckLanguage == "welsh" %>Yn yr adran hon<% else %>In this
                                section<% end_if %></h4>
                        </div>
                        <% loop $Children %>
                            <div class="col-4">
                                <div class="card">
                                    <a href="$Link">
                                        <% if $TeaserImage %>
                                            <% with $TeaserImage.Fill(200,135) %>
                                                <img src="$Link" alt="teaser image" class="card-img-top"/>
                                            <% end_with %>
                                        <% else_if $MainImage %>
                                            <% with $MainImage.Fill(200,135) %>
                                                <img src="$Link" alt="Main image" class="card-img-top"/>
                                            <% end_with %>
                                        <% end_if %>
                                    </a>
                                    <div class="card-body">
                                        <h5 class="card-title"><a href="$Link">$Title</a></h5>
                                        <% if $TeaserText %>
                                            <p class="card-text">$TeaserText</p>
                                        <% end_if %>
                                    </div>
                                </div>

                            </div>
                        <% end_loop %>
                    </div>

                <% end_if %>

            </div>
            <% if $Children || $Parent %>
                <div class="col-sm-3 sidebar">
                    <% include Sidebar %>
                </div>
            <% end_if %>
        </div>
    </div>
</section>
